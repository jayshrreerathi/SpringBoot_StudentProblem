package com.controller;

import com.model.Student;
import com.repository.StudentData;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StudentDataTest {
    /*    Student s1=new Student("Aarti",3,"A");
        Student s2=new Student("Ben",10,"B");
        Student s3=new Student("Pooja",8,"A");
        List<Student> expectedListOfStudents =Arrays.asList(s1, s2, s3);*/
    List<Student> expectedListOfStudents = new ArrayList<>();

    @Test
    public void shouldGetAllStudents() {
        StudentData studentData = new StudentData();
        assertEquals(expectedListOfStudents, studentData.getAllStudents());
    }

    @Test
    public void shouldAddStudent() {
        StudentData studentData = new StudentData();
        Student s = new Student("Jiya", 12, "C");
        expectedListOfStudents.add(s);
        studentData.saveStudent(new Student("Jiya", 12, "C"));
        assertEquals(expectedListOfStudents, studentData.getAllStudents());

    }


}