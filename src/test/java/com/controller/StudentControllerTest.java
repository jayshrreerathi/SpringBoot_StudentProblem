package com.controller;

import com.model.Student;
import com.repository.StudentData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StudentControllerTest {
    @InjectMocks
    private StudentController controller;
    @Mock
    StudentData data;

    List<Student> listOfStudents= new ArrayList<>();
    @Test
    public void shouldGetEmptylistOfStudents() {
        when(data.getAllStudents()).thenReturn(listOfStudents);
        controller.getListofStudent();
        verify(data).getAllStudents();
    }

    @Test
    public void controllerShouldAddStudent(){
        Student student = new Student("Test", 2, "A");
        doNothing().when(data).saveStudent(student);
        controller.addStudent(student);
        verify(data).saveStudent(student);
    }

    @Test
    public void shouldGetListOfAllStudents(){
        Student s1=new Student("Aarti",3,"A");
        Student s2=new Student("Ben",10,"B");
        Student s3=new Student("Pooja",8,"A");
        listOfStudents.add(s1);
        listOfStudents.add(s2);
        listOfStudents.add(s3);
        when(data.getAllStudents()).thenReturn(listOfStudents);
        List<Student> list = controller.getListofStudent();
        assertEquals(listOfStudents,list);
    }

}