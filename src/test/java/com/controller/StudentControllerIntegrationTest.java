package com.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.Student;
import com.repository.StudentData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void studentsShouldReturnEmptyListofStudents() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/students").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    @Test
    public void shouldcreateStudent() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/student").content("{\n" +
                "    \"name\": \"Aarrti\",\n" +
                "    \"standard\": 10,\n" +
                "    \"division\": \"A\"\n" +
                "  }").contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void studentsShouldReturnListOfStudents() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/student").content("{\n" +
                "    \"name\": \"Aarrti\",\n" +
                "    \"standard\": 10,\n" +
                "    \"division\": \"A\"\n" +
                "  }").contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        mvc.perform(MockMvcRequestBuilders.get("/students").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"name\":\"Aarrti\",\"standard\":10,\"division\":\"A\"}]"));
    }
@Test
    public void shouldNotCreateStudentForBadRequest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/student").content("{\n" +
                "\"standard\":3,\n" +
                "\"division\":\"B\"\n" +
                "}").contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

}