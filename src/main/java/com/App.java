package com;

import org.json.simple.parser.ParseException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

/**
 * Hello world!
 *
 */

@SpringBootApplication
public class App 
{
    public static void main( String[] args ) throws IOException, ParseException {
        SpringApplication.run(App.class,args);
        System.out.println( "Hello World!" );

    }
}
