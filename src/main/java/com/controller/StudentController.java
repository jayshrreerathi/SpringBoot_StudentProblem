package com.controller;


import com.model.Student;
import com.repository.StudentData;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
public class StudentController {

    @Autowired
    private StudentData data;

    @RequestMapping(value = "/students", method = RequestMethod.GET, produces = "application/json")
    public List<Student> getListofStudent() {

        return data.getAllStudents();
    }

   @RequestMapping(value="/student",method = RequestMethod.POST)
    public ResponseEntity<String> addStudent(@Valid @RequestBody Student student) {
            data.saveStudent(student);
       return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
