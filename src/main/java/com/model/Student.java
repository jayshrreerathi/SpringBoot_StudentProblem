package com.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Objects;

public class Student {

    @NotNull
    @NotEmpty
    private String name ;
    @NotNull
    @Positive
    private int standard;
    @NotNull
    @NotEmpty
    private String division;

    public Student(){

    }

   public Student(String name, int standard, String division){
        this.name=name;
        this.standard=standard;
        this.division=division;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return standard == student.standard &&
                Objects.equals(name, student.name) &&
                Objects.equals(division, student.division);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, standard, division);
    }

    public String getName() {
        return name;
    }

    public int getStandard() {
        return standard;
    }

    public String getDivision() {
        return division;
    }


}
