package com.repository;



import com.model.Student;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentData {
/*    Student s1=new Student("Aarti",3,"A");
    Student s2=new Student("Ben",10,"B");
    Student s3=new Student("Pooja",8,"A");
    List<Student> listOfStudents= Arrays.asList(new Student[]{s1, s2, s3});*/
List<Student> listOfStudents= new ArrayList<>();

    public List<Student> getAllStudents()  {
        return listOfStudents;
    }

    public void saveStudent(Student s){
        listOfStudents.add(s);
    }
}
